import { motion } from "framer-motion";
import Link from "next/link";

const ExamplePage = ({ title }) => {
  return (
    <div className="relative overflow-hidden">
      <motion.div
        initial={{ opacity: 0, y: -100 }}
        animate={{ opacity: 1, y: 0 }}
        transition={{ duration: 0.4 }}
        className="absolute top-0 left-0 w-full h-full bg-gradient-to-br from-gray-200 to-white z-0">
        <svg
          className="absolute inset-y-0 h-full w-full"
          preserveAspectRatio="none"
          viewBox="0 0 100 100" 
          xmlns="http://www.w3.org/2000/svg">
          <polygon points="0,0 100,0 50,100 0,100" fill="white" />
        </svg>
      </motion.div>

      <div className="relative z-10 flex flex-col items-center justify-center min-h-screen text-center">
        <motion.h1
          initial={{ opacity: 0, y: -50 }}
          animate={{ opacity: 1, y: 0 }}
          transition={{ delay: 0.2 }}
          className="text-4xl font-bold text-gray-800 mb-6">
          {title}
        </motion.h1>
        <Link href="/ideas">
          <motion.div
            className="bg-primary text-white font-bold py-3 px-6 rounded-full shadow-lg transition duration-300"
            initial={{ opacity: 0, y: 50 }}
            animate={{ opacity: 1, y: 0 }}
            transition={{ delay: 0.3 }}>
            Go to Ideas Page
          </motion.div>
        </Link>
      </div>

      <motion.div
        className="absolute top-0 left-0 right-0 bottom-0 overflow-hidden pointer-events-none"
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ delay: 0.8, duration: 1 }}>
        <motion.div
          className="absolute bg-primary-color w-6 h-6 rounded-full"
          style={{ left: "25%", top: "30%" }}
          animate={{
            rotate: [0, 360],
            scale: [1, 0.5, 1],
            opacity: [1, 0.7, 1],
            transition: { repeat: Infinity, duration: 4 },
          }}
        />
        <motion.div
          className="absolute bg-primary-color w-8 h-8 rounded-full"
          style={{ left: "60%", top: "70%" }}
          animate={{
            rotate: [0, -360],
            scale: [1, 0.8, 1],
            opacity: [1, 0.6, 1],
            transition: { repeat: Infinity, duration: 5 },
          }}
        />
      </motion.div>
    </div>
  );
};

export default ExamplePage;
