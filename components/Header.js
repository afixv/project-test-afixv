import { useState, useEffect, useRef } from "react";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { motion } from "framer-motion";

const Header = () => {
  const [visible, setVisible] = useState(true);
  const [headerOpacity, setHeaderOpacity] = useState(1);
  const [isMobileMenuOpen, setIsMobileMenuOpen] = useState(false);
  const router = useRouter();

  let lastScrollTop = useRef(0);

  useEffect(() => {
    const handleScroll = () => {
      const scrollTop = window.scrollY || document.documentElement.scrollTop;
      if (scrollTop > lastScrollTop.current) {
        setVisible(false);
      } else {
        setVisible(true);
      }
      lastScrollTop.current = scrollTop;

      if (scrollTop > 400) {
        setHeaderOpacity(0.7);
      } else {
        setHeaderOpacity(1);
      }
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const pages = [
    { name: "Work", href: "/work" },
    { name: "About", href: "/about" },
    { name: "Services", href: "/services" },
    { name: "Ideas", href: "/ideas" },
    { name: "Careers", href: "/careers" },
    { name: "Contact", href: "/contact" },
  ];

  const toggleMobileMenu = () => {
    setIsMobileMenuOpen(!isMobileMenuOpen);
  };

  return (
    <header
      className={`fixed z-50 top-0 p-5 w-full bg-primary transition-all duration-300 ${
        visible ? "translate-y-0" : "-translate-y-full"
      }`}
      style={{ opacity: headerOpacity }}>
      <nav className="mx-auto items-center flex justify-between max-w-[1200px]">
        <Link href="/">
          <Image
            src="/suitmedialogobgremoved.png"
            alt="Suitmedia Logo"
            width={100}
            height={50}
          />
        </Link>

        <ul className="hidden md:flex space-x-6">
          {pages.map((page) => (
            <li key={page.name}>
              <Link
                href={page.href}
                className="relative font-normal text-white pb-2 inline-block hover:opacity-80">
                <span className="relative z-10">{page.name}</span>
                <motion.span
                  className="absolute inset-x-0 bottom-0 h-0.5 rounded-full bg-white origin-center"
                  initial={{ scaleX: 0 }}
                  animate={{
                    scaleX: router.pathname === page.href ? 1 : 0,
                    transition: { duration: 0.3 },
                  }}
                />
              </Link>
            </li>
          ))}
        </ul>

        <div className="md:hidden">
          <button
            className="text-white focus:outline-none transition-opacity duration-300"
            onClick={toggleMobileMenu}>
            <svg
              className={`w-6 h-6 transition-transform duration-300 ${
                isMobileMenuOpen ? "rotate-90" : ""
              }`}
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor">
              {isMobileMenuOpen ? (
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M6 18L18 6M6 6l12 12"
                />
              ) : (
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M4 6h16M4 12h16m-7 6h7"
                />
              )}
            </svg>
          </button>

          <ul
            className={`absolute top-[80px] w-52 rounded-2xl duration-300 transition-all right-5 bg-primary gap-6 py-8 flex flex-col items-center ${
              isMobileMenuOpen
                ? "translate-y-0 opacity-100"
                : "opacity-0 -translate-y-[400px]"
            }`}>
            {pages.map((page) => (
              <li key={page.name}>
                <Link
                  href={page.href}
                  className={`text-white py-2 ${
                    router.pathname === page.href
                      ? "border-b-[3px] border-white"
                      : ""
                  }`}>
                  {page.name}
                </Link>
              </li>
            ))}
          </ul>
        </div>
      </nav>
    </header>
  );
};

export default Header;
