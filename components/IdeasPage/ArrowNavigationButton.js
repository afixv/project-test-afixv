const ArrowNavigationButton = ({ disabled, children, onClick }) => {
  return (
    <button
      className="px-3 mx-1 text-lg disabled:text-slate-400 disabled:hover:text-slate-400 font-semibold py-2 rounded-lg text-slate-950 hover:text-primary"
      onClick={onClick}
      disabled={disabled}>
      {children}
    </button>
  );
};

export default ArrowNavigationButton;
