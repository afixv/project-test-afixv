import {
  ParallaxBanner,
  Parallax,
  ParallaxBannerLayer,
} from "react-scroll-parallax";
import { useState } from "react";
import { motion } from "framer-motion";

const Banner = ({ title, subtitle, bannerImg, onError }) => {
  const [isLoaded, setIsLoaded] = useState(false);

  const handleImageLoad = () => {
    setIsLoaded(true);
  };
  return (
    <ParallaxBanner
      className="relative h-[720px] overflow-hidden "
      style={{ clipPath: "polygon(0 0, 100% 0%, 100% 60%, 0% 100%)" }}>
      <ParallaxBannerLayer speed={50}>
        <img
          className={`object-cover w-full h-full transition-opacity duration-300 ${
            isLoaded ? "opacity-100" : "opacity-0"
          }`}
          src={bannerImg}
          alt="Banner Image"
          loading="lazy"
          onLoad={handleImageLoad}
          onError={onError ? onError : ""}
        />
      </ParallaxBannerLayer>
      <div
        className="absolute inset-0 bg-black opacity-70"
        style={{ clipPath: "polygon(0 0, 100% 0%, 100% 60%, 0% 100%)" }}
      />
      <Parallax speed={-30}>
        <div className="absolute inset-0 flex items-center justify-center flex-col px-8">
          <motion.h1
            initial={{ y: -50, opacity: 0 }}
            animate={{ y: 0, opacity: 1 }}
            className=" text-white  text-center text-5xl">
            {title}
          </motion.h1>
          <motion.h3
            initial={{ y: 50, opacity: 0 }}
            animate={{ y: 0, opacity: 1 }}
            transition={{ delay: 0.3 }}
            className="mt-4 text-white">
            {subtitle}
          </motion.h3>
        </div>
      </Parallax>
    </ParallaxBanner>
  );
};

export default Banner;
