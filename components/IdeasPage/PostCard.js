import Image from "next/image";
import Link from "next/link";
import { useState } from "react";

const PostCard = ({ post }) => {
  const [imageSrc, setImageSrc] = useState(
    post.small_image && post.small_image[0] && post.small_image[0].url
  );  

  const handleImageError = () => {
    if (imageSrc !== "/img/thumbnail.png") {
      setImageSrc("/img/thumbnail.png");
    }
  };
  return (
    <Link
      href={`/ideas/${post.slug}`}
      className="rounded-lg shadow-xl w-64 transition hover:-translate-y-2">
      <div className="relative h-[180px] w-full">
        <Image
          src={imageSrc}
          onError={handleImageError}
          width={256}
          height={144}
          className="rounded-t-lg text-slate-500 text-sm"
          alt={post.title}
          loading="lazy"
        />
      </div>
      <div className="p-4">
        <p className="text-sm text-gray-500">
          {new Date(post.published_at).toLocaleDateString("id-ID", {
            day: "numeric",
            month: "long",
            year: "numeric",
          })}
        </p>
        <h3 className="text-md leading-tight text-slate-950 font-semibold line-clamp-3">
          {post.title}
        </h3>
      </div>
    </Link>
  );
};

export default PostCard;
