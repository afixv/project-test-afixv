const Loading = () => {
  return (
    <div className="flex justify-center items-center h-screen item-center bg-slate-50">
      <div className="animate-spin rounded-full h-12 w-12 border-t-2 border-primary"></div>
    </div>
  );
};

export default Loading;
