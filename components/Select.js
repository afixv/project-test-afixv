const Select = ({ id, label, options, value, onChange }) => {
  return (
    <>
      <label
        htmlFor={id}
        className="mx-4 text-sm font-semibold text-slate-950">
        {label}
      </label>
      <select
        id={id}
        className="w-32 px-4 py-2 text-sm font-semibold border rounded-3xl text-slate-950"
        value={value}
        onChange={onChange}>
        {options.map((option) => (
          <option key={option.value} value={option.value}>
            {option.label}
          </option>
        ))}
      </select>
    </>
  );
};

export default Select;
