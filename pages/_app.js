import "@/styles/globals.css";
import Header from "@/components/Header";
import { ParallaxProvider } from "react-scroll-parallax";

export default function App({ Component, pageProps }) {
  return (
    <ParallaxProvider>
      <Header />
      <Component {...pageProps} />
    </ParallaxProvider>
  );
}
