import { ExamplePage } from "@/components";

const About = () => {
  return <ExamplePage title="About" />;
};

export default About;
