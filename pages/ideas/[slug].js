import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import axios from "axios";
import Image from "next/image";
import { Banner, Loading } from "@/components";

export default function PostPage() {
  const router = useRouter();
  const { slug } = router.query;
  const [postData, setPostData] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [imageSrc, setImageSrc] = useState("/img/thumbnail.png");

  const fetchData = async () => {
    setLoading(true);
    try {
      const response = await axios.get(`/api/ideas/${slug}`, {
        params: {
          append: ["medium_image"],
        },
      });
      setPostData(response.data.data);
    } catch (error) {
      setError(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    if (slug) {
      fetchData();
    }
  }, [slug]);

  useEffect(() => {
    if (postData && postData.medium_image && postData.medium_image[0]) {
      setImageSrc(postData.medium_image[0].url);
    }
  }, [postData]);

  const handleImageError = () => {
    if (imageSrc !== "/img/thumbnail.png") {
      setImageSrc("/img/thumbnail.png");
    }
  };

  if (loading) {
    return <Loading />;
  }

  if (error) {
    return <div>Error: {error.message}</div>;
  }

  if (!postData) {
    return <div>No post found for "{slug}"</div>;
  }

  return (
    <div className="bg-slate-50 py-12">
      <Banner
        title={postData.title}
        subtitle={new Date(postData.published_at).toLocaleDateString("id-ID", {
          day: "numeric",
          month: "long",
          year: "numeric",
        })}
        bannerImg={imageSrc}
        onError={handleImageError}
      />
      <div className="min-h-screen flex-col justify-center items-center flex bg-slate-50 px-8 text-slate-900 md:mx-auto max-w-[800px]">
        <div
          className="items-center text-left justify-center flex flex-col gap-6 text-normal text-slate-900"
          dangerouslySetInnerHTML={{ __html: postData.content }}
        />
      </div>
    </div>
  );
}
