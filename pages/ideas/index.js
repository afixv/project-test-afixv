import { useState, useEffect } from "react";
import axios from "axios";
import Cookies from "js-cookie";
import {
  PostCard,
  Banner,
  Loading,
  Select,
  ArrowNavigationButton,
} from "@/components";

const Ideas = () => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const [sort, setSort] = useState("-published_at");
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const cookiePage = Cookies.get("page");
    const cookiePageSize = Cookies.get("pageSize");
    const cookieSort = Cookies.get("sort");

    if (cookiePage) setPage(Number(cookiePage));
    if (cookiePageSize) setPageSize(Number(cookiePageSize));
    if (cookieSort) setSort(cookieSort);
  }, []);

  const fetchData = async (page, pageSize, sort) => {
    setLoading(true);
    try {
      const response = await axios.get(`/api/ideas`, {
        params: {
          "page[number]": page,
          "page[size]": pageSize,
          sort: sort,
          append: ["small_image"],
        },
      });
      setData(response.data);
    } catch (error) {
      setError(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchData(page, pageSize, sort);
  }, [page, pageSize, sort]);

  const updateCookies = (newPage, newPageSize, newSort) => {
    Cookies.set("page", newPage, { expires: 7 });
    Cookies.set("pageSize", newPageSize, { expires: 7 });
    Cookies.set("sort", newSort, { expires: 7 });
  };

  useEffect(() => {
    updateCookies(page, pageSize, sort);
  }, [page, pageSize, sort]);

  if (error)
    return (
      <div className="text-slate-900 bg-slate-50 h-screen justify-center items-center flex">
        Failed to load
      </div>
    );
  if (!data) return <Loading />;
  if (loading) return <Loading />;

  const handleSortChange = (e) => {
    const newSort = e.target.value;
    setSort(newSort);
    setPage(1);
  };

  const handlePageSizeChange = (e) => {
    const newPageSize = Number(e.target.value);
    setPageSize(newPageSize);
    setPage(1);
  };

  const navigateToPage = (pageNumber) => {
    setPage(pageNumber);
    window.scrollTo({
      top: 600,
      behavior: "smooth",
    });
  };

  const navigateToPageUrl = (url) => {
    const pageNumber = new URLSearchParams(new URL(url).search).get(
      "page[number]"
    );
    setPage(Number(pageNumber));
    window.scrollTo({
      top: 400,
      behavior: "smooth",
    });
  };

  const sortOptions = [
    { label: "Newest", value: "-published_at" },
    { label: "Oldest", value: "published_at" },
  ];

  const pageSizeOptions = [10, 20, 50];

  return (
    <main className="flex bg-gray-50 flex-col pb-24">
      <Banner
        title="Ideas"
        subtitle="Where all our great thing begin"
        bannerImg="/img/banner.png"
      />
      <div className="max-w-[1200px] mx-auto">
        <div className="my-8 flex flex-col gap-2 md:flex-row justify-between items-center">
          <span className="text-sm font-semibold text-slate-950">
            Showing {data.meta.from} - {data.meta.to} of {data.meta.total} ideas
          </span>
          <div className="flex md:block flex-col items-center gap-2">
            <Select
              id="sort"
              label="Sort By:"
              options={sortOptions}
              value={sort}
              onChange={handleSortChange}
            />
            <Select
              id="pageSize"
              label="Page Size:"
              options={pageSizeOptions.map((size) => ({
                label: size,
                value: size,
              }))}
              value={pageSize}
              onChange={handlePageSizeChange}
            />
          </div>
        </div>
        <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 xl:grid-cols-4 gap-4">
          {data.data.map((post) => (
            <PostCard key={post.id} post={post} />
          ))}
        </div>
        <div className="flex justify-center items-center mt-16">
          <ArrowNavigationButton
            onClick={() => navigateToPageUrl(data.links.first)}
            disabled={!data.links.first || data.meta.current_page === 1}>
            &laquo;
          </ArrowNavigationButton>
          <ArrowNavigationButton
            onClick={() => navigateToPageUrl(data.links.prev)}
            disabled={!data.links.prev}>
            &#8249;
          </ArrowNavigationButton>
          {data.meta.links.map(
            (link, index) =>
              !link.label.includes("Previous") &&
              !link.label.includes("Next") && (
                <button
                  key={index}
                  className={`px-3 hidden lg:block mx-1 text-sm font-semibold py-2 rounded-lg ${
                    link.active
                      ? "bg-primary hover:bg-primary text-white"
                      : "text-slate-950 hover:text-primary"
                  }`}
                  onClick={() => navigateToPage(link.label)}
                  disabled={!link.url}>
                  {link.label}
                </button>
              )
          )}
          <ArrowNavigationButton
            onClick={() => navigateToPageUrl(data.links.next)}
            disabled={!data.links.next}>
            &#8250;
          </ArrowNavigationButton>
          <ArrowNavigationButton
            onClick={() => navigateToPageUrl(data.links.last)}
            disabled={
              !data.links.last || data.meta.current_page === data.meta.last_page
            }>
            &raquo;
          </ArrowNavigationButton>
        </div>
      </div>
    </main>
  );
};

export default Ideas;
