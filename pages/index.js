import { ExamplePage } from "@/components";

const HomePage = () => {
  return <ExamplePage title="Home" />;
};

export default HomePage;
